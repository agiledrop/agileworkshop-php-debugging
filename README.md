# Drupal/PHP Debugging Workshop

### Project installation

- Clone this repository
- Run command `lando start` from project root
- Run command `lando composer install`
- Run command `lando db-import ./database.sql.gz`

### Using drush
- Run command `lando drush "command"`
