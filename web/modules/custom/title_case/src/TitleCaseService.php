<?php

namespace Drupal\title_case;

class TitleCaseService {
  public function convertTitle($title){
    $noPrincipal = ['a','an','the','in','with','by','of','on','and','or','but'];
    $titleCaseTitle = explode(' ', $title);
    foreach ($titleCaseTitle as $key => $word) {
      if (!in_array($word, $noPrincipal)){
        $titleCaseTitle[$key] = ucfirst($word);
      }
    }
    $titleCaseTitle = implode(' ', $titleCaseTitle);
    return $titleCaseTitle != $title ?? $titleCaseTitle;
  }
}
