<?php

namespace Drupal\car\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the car entity edit forms.
 */
class CarForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();

    if (is_null($entity->price->value)) {
      $entity->setStatus(false);
    }

    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New car %label has been created.', $message_arguments));
      $this->logger('car')->notice('Created new car %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The car %label has been updated.', $message_arguments));
      $this->logger('car')->notice('Updated new car %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.car.canonical', ['car' => $entity->id()]);
  }

}
