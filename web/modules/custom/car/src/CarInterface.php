<?php

namespace Drupal\car;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a car entity type.
 */
interface CarInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the car title.
   *
   * @return string
   *   Title of the car.
   */
  public function getTitle();

  /**
   * Sets the car title.
   *
   * @param string $title
   *   The car title.
   *
   * @return \Drupal\car\CarInterface
   *   The called car entity.
   */
  public function setTitle($title);

  /**
   * Gets the car creation timestamp.
   *
   * @return int
   *   Creation timestamp of the car.
   */
  public function getCreatedTime();

  /**
   * Sets the car creation timestamp.
   *
   * @param int $timestamp
   *   The car creation timestamp.
   *
   * @return \Drupal\car\CarInterface
   *   The called car entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the car status.
   *
   * @return bool
   *   TRUE if the car is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the car status.
   *
   * @param bool $status
   *   TRUE to enable this car, FALSE to disable.
   *
   * @return \Drupal\car\CarInterface
   *   The called car entity.
   */
  public function setStatus($status);

}
